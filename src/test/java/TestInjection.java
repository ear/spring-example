import cz.cvut.kbss.ear.spring_example.AppConfig;
import cz.cvut.kbss.ear.spring_example.CourseRepository;
import cz.cvut.kbss.ear.spring_example.SchoolInformationSystem;
import cz.cvut.kbss.ear.spring_example.SchoolInformationSystemWithTwoRepositories;
import java.text.MessageFormat;
import java.util.logging.Logger;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

/**
 * TestInjection - a class showing basic DI use-cases.
 *
 * 1. testInjectBean & testGetBeanThroughApplicationContext
 *      - shows normal operation of Spring DI - injected using annotations or fetched
 *        from the application context
 * 2. testCreateBean
 *      - shows that on creation a bean class instance outside Spring container, no dependencies
 *        are injected
 * 3. testTwoRepositories
 *      - shows how to use scopes and qualifiers to determine which bean to inject
 * 4. try to comment out both @Qualifier annotations in
 *    cz.cvut.kbss.ear.spring_example.SchoolInformationSystemWithTwoRepositories. The bean is
 *    even not instantiated as spring cannot determine, which CourseRepository implementation
 *    to inject.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {AppConfig.class})
public class TestInjection {

    private static final Logger LOG = Logger.getLogger(TestInjection.class.getName());

    @Autowired
    private ApplicationContext context;

    @Autowired
    private SchoolInformationSystem injectedBean;

    public void logCourses(final CourseRepository r) {
        LOG.info(MessageFormat.format("Courses are {0} ",r.getCourses()));
    }

    @Test
    public void testInjectBean() {
        logCourses(injectedBean.getRepository());
        Assert.assertEquals(3, injectedBean.getRepository().getCourses().size());
    }

    @Test
    public void testGetBeanThroughApplicationContext() {
        final SchoolInformationSystem bean = context.getBean(SchoolInformationSystem.class);
        logCourses(bean.getRepository());
        Assert.assertEquals(3,bean.getRepository().getCourses().size());
    }

    @Test(expected = NullPointerException.class)
    public void testCreateBean() {
        SchoolInformationSystem main = new SchoolInformationSystem();
        logCourses(main.getRepository());
    }

    @Test
    public void testTwoRepositories() {
        final SchoolInformationSystemWithTwoRepositories main = context.getBean(SchoolInformationSystemWithTwoRepositories.class);
        logCourses(main.getRepository1());
        logCourses(main.getRepository2());
        // Only succeeds if InMemoryCourseRepository is a singleton
        Assert.assertEquals(main.getRepository1().getCourses(),main.getRepository2().getCourses());
    }

    @Test
    public void testCriteria() {
        logCourses(injectedBean.getRepository());
    }
}