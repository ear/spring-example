DELETE FROM course;
DELETE FROM person;

INSERT INTO person ( id, name ) VALUES (1,'John');
INSERT INTO person ( id, name ) VALUES (2,'Sam');

INSERT INTO course ( id, name, teacher_id ) VALUES (1,'EAR',1);
INSERT INTO course ( id, name, teacher_id ) VALUES (2,'WPA',1);
INSERT INTO course ( id, name, teacher_id ) VALUES (3,'Math1',2);