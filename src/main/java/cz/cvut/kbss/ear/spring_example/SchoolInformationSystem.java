package cz.cvut.kbss.ear.spring_example;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

@Service
@Scope("singleton")
public class SchoolInformationSystem {

    @Autowired
    private CourseRepository dbCourseRepository;

    public CourseRepository getRepository() {
        return dbCourseRepository;
    }
}
