package cz.cvut.kbss.ear.spring_example;

import cz.cvut.kbss.ear.spring_example.entities.Course;
import java.util.List;

public interface CourseRepository {

    List<Course> getCourses();
}
