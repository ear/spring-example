package cz.cvut.kbss.ear.spring_example;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

@Service
public class SchoolInformationSystemWithTwoRepositories {

    @Autowired
    @Qualifier("inMemoryRepo")
    private CourseRepository repository;

    @Autowired
    @Qualifier("inMemoryRepo")
    private CourseRepository repository2;

    public CourseRepository getRepository1() {
        return repository;
    }
    public CourseRepository getRepository2() {
        return repository2;
    }
}
