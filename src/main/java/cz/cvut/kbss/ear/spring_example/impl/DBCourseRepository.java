package cz.cvut.kbss.ear.spring_example.impl;

import cz.cvut.kbss.ear.spring_example.*;
import cz.cvut.kbss.ear.spring_example.entities.Course;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Repository;

@Repository("dbCourseRepository")
@Scope("singleton")
public class DBCourseRepository implements CourseRepository {
    
    @PersistenceContext
    private EntityManager em;
    
    @Override
    public List<Course> getCourses() {
        return em.createQuery("SELECT c FROM Course c").getResultList();
    }
}