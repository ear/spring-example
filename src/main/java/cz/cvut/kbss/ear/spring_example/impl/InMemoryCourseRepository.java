package cz.cvut.kbss.ear.spring_example.impl;

import cz.cvut.kbss.ear.spring_example.entities.Person;
import cz.cvut.kbss.ear.spring_example.*;
import cz.cvut.kbss.ear.spring_example.entities.Course;
import java.util.ArrayList;
import java.util.List;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Repository;

@Repository("inMemoryRepo")
@Scope("singleton")
//@Scope("prototype")
public class InMemoryCourseRepository implements CourseRepository {

    private static int current = 0;

    private final List<Course> courses;

    public InMemoryCourseRepository() {
        courses = new ArrayList<>();
        
        Person t1 = new Person(1, "John");
        Person t2 = new Person(1, "Sam");
        
        courses.add(new Course(1,"Math"+current,t1));
        courses.add(new Course(2,"Java",t2));

        current++;
    }

    @Override
    public String toString() {
        return getClass().getName() + ": courses=" + courses + '}';
    }

    @Override
    public List<Course> getCourses() {
        return courses;
    }
}